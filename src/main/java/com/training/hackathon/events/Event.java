package com.training.hackathon.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Event {
    private static final Logger LOG = LoggerFactory.getLogger(Event.class);

    @Autowired
    private static Environment env;

    private Long tradeID;
    private EventType eventType;
//    private Map<String, String> payload;

    public static void emitEvent(Long tradeID, EventType eventType) {
//        Map<String, String> payloadMap = new HashMap<>();
//        payloadMap.put("FROM", from);
//        payloadMap.put("TO", to);
        Event event = new Event(tradeID, eventType);
        try {
            handleHttp(event);
        } catch(IOException ex) {
            LOG.error("IOException: " + ex.toString());
        } catch (InterruptedException ex) {
            LOG.error("InterruptedException: " + ex.toString());
        }

    }

    private static void handleHttp(Event event) throws IOException, InterruptedException {

        var objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writeValueAsString(event);
        System.out.println(requestBody);
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://events-service-lum-events-service-lum.emeadocker13.conygre.com/api/v1/events"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());

        LOG.debug("Trades Events updated: " + event.toString());
        LOG.debug("Response: " + response.body());

        System.out.println(response.body());
    }

    public Event(Long tradeID, EventType eventType) {
        this.tradeID = tradeID;
        this.eventType = eventType;
    }

    public Long getTradeID() {
        return tradeID;
    }

    public void setTradeID(Long tradeID) {
        this.tradeID = tradeID;
    }

    public EventType getEventType() {
        return eventType;
    }

    @Override
    public String toString() {
        return "Event{" +
                "tradeID=" + tradeID +
                ", eventType=" + eventType +
                '}';
    }
}
