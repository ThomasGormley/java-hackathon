package com.training.hackathon.trading.services;

import com.training.hackathon.events.Event;
import com.training.hackathon.events.EventType;
import com.training.hackathon.trading.entities.Trade;
import com.training.hackathon.trading.repositories.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeService {

    @Autowired
    private TradeRepository tradeRepository;

    public List<Trade> findAll() {
        return tradeRepository.findAll();
    }

    public Trade findById(long id) {
        return tradeRepository.findById(id).get();
    }

    public List<Trade> getTradesByPriceBetween(double min, double max) {
        return tradeRepository.getTradesByPriceBetween(min, max);
    }

    public List<Trade> getAllByStatusCodeEquals(int StatusCode) {
        return tradeRepository.getAllByStatusCodeEquals(StatusCode);
    }

    public Trade save(Trade trade) {
        EventType buyOrSellEvent = trade.getBuyOrSell().equalsIgnoreCase("buy") ? EventType.TRADE_BUY : EventType.TRADE_SELL;
        Trade savedTrade = tradeRepository.save(trade);
        Event.emitEvent(savedTrade.getId(), buyOrSellEvent);
        return savedTrade;
    }

    public Trade update(Trade trade) {
        tradeRepository.findById(trade.getId()).get();
        return tradeRepository.save(trade);
    }

    public Trade updateStatusById(long id, int status) {
        Trade trade = tradeRepository.findById(id).get();
        trade.setStatusCode(status);
        return tradeRepository.save(trade);
    }

    public void delete(long id) {
        tradeRepository.deleteById(id);
    }


    public List<Trade> findByStockTicker(String stockTicker) {
        return tradeRepository.getAllByStockTickerEquals(stockTicker);
    }


    public List<Trade> findByVolume(int volume) {
        return tradeRepository.getAllByVolumeEquals(volume);
    }


}
