package com.training.hackathon.trading.entities;

import javax.persistence.*;
import java.util.Date;

@Entity(name="trades")
public class Trade {

    private java.util.Date date = new Date();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // the equivalent of auto increment - don't get dups
    @Column(name = "StockID")
    private Long id;

    @Column(name = "StockTicker")
    private String stockTicker;

    @Column(name = "Price")
    private double price;

    @Column(name = "Volume")
    private int volume;

    @Column(name = "BuyOrSell")
    private String buyOrSell;

    @Column(name = "StatusCode")
    private int statusCode;

    @Column(name = "CreatedTime")
    private Date createdTime = new java.sql.Timestamp(date.getTime());

    @Column(name = "UpdatedTime")
    private Date updatedTime;

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(String buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "id=" + id +
                ", stockTicker='" + stockTicker + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                ", buyOrSell='" + buyOrSell + '\'' +
                ", statusCode=" + statusCode +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                '}';
    }

}
