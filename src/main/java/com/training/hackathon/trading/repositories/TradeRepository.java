package com.training.hackathon.trading.repositories;

import com.training.hackathon.trading.entities.Trade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeRepository extends JpaRepository<Trade, Long> {
    List<Trade> getTradesByPriceBetween(double min, double max);

    List<Trade> getAllByStockTickerEquals(String stockTicker);

    List<Trade> getAllByStatusCodeEquals(int StatusCode);

    // getAllBy control space to see options
    // NOTE has to be a list if responses are not going to be unique
    List<Trade> getAllByVolumeEquals(int volume);

}





