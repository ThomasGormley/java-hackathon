package com.training.hackathon.trading.controllers;

import com.training.hackathon.trading.entities.Trade;
import com.training.hackathon.trading.services.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/trade")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeService tradeService;

    @GetMapping
    public List<Trade> findAll() {
        return tradeService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Trade> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<Trade>(tradeService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}/status")
    public ResponseEntity<Trade> updateStatusById(@PathVariable long id, @RequestBody int status) {
        try {
            return new ResponseEntity<Trade>(tradeService.updateStatusById(id, status), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/price")
    public ResponseEntity<List<Trade>> findBetweenPrices(@RequestParam double min, @RequestParam double max) {
        try {
            return new ResponseEntity<List<Trade>>(tradeService.getTradesByPriceBetween(min, max), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/statuscode/{StatusCode}")
    public ResponseEntity<List<Trade>> findAllByStatusCode(@PathVariable int StatusCode) {

        try {
            return new ResponseEntity<>(tradeService.getAllByStatusCodeEquals(StatusCode), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Trade> create(@RequestBody Trade trade) {
        try {
            return new ResponseEntity<Trade>(tradeService.save(trade), HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping
    public ResponseEntity<Trade> update(@RequestBody Trade trade) {
        try {
            return new ResponseEntity<Trade>(tradeService.update(trade), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            LOG.debug("update for unknown id: [" + trade + "]");
            return new ResponseEntity<Trade>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        try {
            tradeService.delete(id);
        } catch (EmptyResultDataAccessException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("ticker/{stockTicker}")
    public ResponseEntity<List<Trade>> findByStockTicker(@PathVariable String stockTicker) {
        try {
            return new ResponseEntity<List<Trade>>(tradeService.findByStockTicker(stockTicker), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/volume/{volume}")
    // needs to be a different endpoint /volume/...
    // wrapping trade in a list
    public ResponseEntity<List<Trade>> findByVolume(@PathVariable int volume) {
        try {
            return new ResponseEntity<List<Trade>>(tradeService.findByVolume(volume), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
