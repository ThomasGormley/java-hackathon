package com.training.hackathon.trading.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TradesTest {

    // constants
    private static final String testStockTicker = "TickerTest";
    private static final double testPrice = 250.00;
    private static final int testVolume = 5;
    private static final String testBuyOrSell = "buy";
    private static final int testStatusCode = 10;

    private Trade trade;

    //Date myDate = new Date();
    //private final Date testCreatedTime = myDate.getTime();

    @BeforeEach
    public void setup() {
        this.trade = new Trade();
    }

    @Test
    public void setGetStockTickerTest() {
        this.trade.setStockTicker(testStockTicker);
        assertEquals(this.trade.getStockTicker(), testStockTicker);
    }

    @Test
    public void setGetPriceTest() {
        this.trade.setPrice(testPrice);
        assertEquals(this.trade.getPrice(), testPrice);
    }

    @Test
    public void setGetVolumeTest() {
        this.trade.setVolume(testVolume);
        assertEquals(this.trade.getVolume(), testVolume);
    }

    @Test
    public void setGetBuyOrSellTest() {
        this.trade.setBuyOrSell(testBuyOrSell);
        assertEquals(this.trade.getBuyOrSell(), testBuyOrSell);
    }

    @Test
    public void setGetStatusCodeTest() {
        this.trade.setStatusCode(testStatusCode);
        assertEquals(this.trade.getStatusCode(), testStatusCode);
    }

//    @Test
//    public void setGetCreatedTimeTest() {
//        this.trade.setCreatedTime(testCreatedTime);
//        assertEquals(this.trade.getCreatedTime(), testCreatedTime);
//    }
//
//    @Test
//    public void setGetUpdatedTimeTest() {
//        this.trade.setUpdatedTime(testUpdatedTime);
//        assertEquals(this.trade.getUpdatedTime(), testUpdatedTime);
//    }

}
