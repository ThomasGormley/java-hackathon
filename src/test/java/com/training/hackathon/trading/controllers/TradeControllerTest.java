package com.training.hackathon.trading.controllers;

import com.training.hackathon.trading.entities.Trade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class TradeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void tradeControllerFindAllSuccess() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trade"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void tradeControllerFindTickerSuccess() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trade/ticker/AMZN"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }


    @Test
    public void shipperControllerFindAllNotFound() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/tradesz"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andReturn();
    }
}




